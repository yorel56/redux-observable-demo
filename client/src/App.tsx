import React, { useRef, useState } from 'react';
import './App.css';
import { Provider, useDispatch, useSelector } from "react-redux";
import { configureStore } from './store';
import { giphyActionCreator } from './GiphyActionCreator';
import { giphySelector } from './GiphySelector';
import { Container, Row, Col, Image, Form, InputGroup, FormControl, Button } from 'react-bootstrap';


const SearchBar = () => {
  const dispatch = useDispatch();
  const searchRef = useRef<HTMLInputElement>(null)
  const [searchForGifs, setSearchForGifs] = useState(true);
  return (<Form onSubmit={(e) => {
    e.preventDefault();
    const searchText = searchRef.current?.value ?? "";
    if(!searchText) {
      return;
    }
    const search = searchForGifs ? giphyActionCreator.fetchGifs : giphyActionCreator.fetchStickers;
    dispatch(search(searchText))
  }}>
    <Row>
      <Col xs={10}>  
        <InputGroup className="mb-3">
          <InputGroup.Text >Search</InputGroup.Text>
            <FormControl
              placeholder="Search"
              ref={searchRef}
              type="text"
            />
            <Button variant="outline-success" type="submit">
              Search
            </Button>
        </InputGroup>
      </Col>
      <Col>  
        <Form.Check 
          type="switch"
          label={searchForGifs ? "Search for gifs" : "Search for stickers"}
          checked={searchForGifs}
          onChange={() => setSearchForGifs(!searchForGifs)}
        />
      </Col>
    </Row>
  </Form>);
}

const GifRows = () => {
  const gifs = useSelector(giphySelector.selectGifs);
  return(<>
  {gifs.map((gif, index) => {
    return (<Row key={index}>
      <Col>
        <Image style={{maxHeight: 120 }} src={gif} fluid/>
      </Col>
    </Row>)
  })}
  </>)
}
const StickerRows = () => {
  const stickers = useSelector(giphySelector.selectStickers);
  return(<>
  {stickers.map((sticker, index) => {
    return (<Row key={index}>
      <Col>
        <Image style={{maxHeight: 120 }} src={sticker} fluid/>
      </Col>
    </Row>)
  })}
  </>)
}

function App() {
  return (
    <Provider store={configureStore()}>
      <Container className="App" style={{marginBottom: 120}}>
        <Row style={{marginTop: 120}}>
          <Col>
            <SearchBar />
          </Col>
        </Row>
        <Row>
          <Col>
            <Row>
              <Col>
                <h2>Gifs</h2>
              </Col>
            </Row>
          <GifRows />
          </Col>
          <Col>
            <Row>
              <Col>
                <h2>Stickers</h2>
              </Col>
            </Row>
          <StickerRows />
          </Col>
        </Row>
      </Container>
    </Provider>
  );
}

export default App;
