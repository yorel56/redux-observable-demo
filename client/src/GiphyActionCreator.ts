import { GiphyActionTypes, SetGifsAction, FetchGifsAction, SetStickersAction, FetchStickersAction } from './GiphyReduxTypes';


class GiphyActionCreator {
    public setGifs = (payload: string[]): SetGifsAction => ({
        payload,
        type: GiphyActionTypes.SET_GIFS
    })
    public fetchGifs = (payload: string): FetchGifsAction => ({
        payload,
        type: GiphyActionTypes.FETCH_GIFS
    })
    public setStickers = (payload: string[]): SetStickersAction => ({
        payload,
        type: GiphyActionTypes.SET_STICKERS
    })
    public fetchStickers = (payload: string): FetchStickersAction => ({
        payload,
        type: GiphyActionTypes.FETCH_STICKERS
    })
}

export const giphyActionCreator = new GiphyActionCreator();