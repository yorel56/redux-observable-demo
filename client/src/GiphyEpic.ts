import { Action } from 'redux';
import { Observable } from 'rxjs';
import { giphyActionCreator } from './GiphyActionCreator';
import { from } from 'rxjs';
import { GiphyActionTypes, FetchGifsAction, FetchStickersAction } from './GiphyReduxTypes';
import { Epic, ofType } from "redux-observable";
import { IEpic } from "./IEpic";
import * as GiphyApi from "giphy-api";
import {map, mergeMap} from "rxjs/operators"
import { createAction } from 'typesafe-actions';
import axios from 'axios';

const API_KEY = ""

const giphyApi = GiphyApi.default(API_KEY)

const fetchGifs = (searchText: string) => {
    return giphyApi.search({
            limit: 5,
            q: searchText,
            rating: "g",
            offset: Math.floor(Math.random() * 100)
    }).then(({data}) => {
        return data.map((obj) => obj.images.original.url);
    })
}
const fetchStickers = (searchText: string) => {
    return axios.get<{data: {images: {original: {url: string}}}[]}>("https://api.giphy.com/v1/stickers/search", {
        params:{
            api_key: API_KEY,
            limit: 5,
            q: searchText,
            rating: "g",
            offset: Math.floor(Math.random() * 100)
        }
    }).then(({data}) => {
        return data.data.map((obj) => obj.images.original.url);
    })
}
export const setGifsAction = createAction(
  GiphyActionTypes.SET_GIFS,
  (resolve) => (gifs: string[]) => resolve(gifs)
);

class GiphyEpic implements IEpic {

    public fetchGifs = (action$: Observable<Action>) => {
        return action$.pipe(
            ofType(GiphyActionTypes.FETCH_GIFS) as any,
            mergeMap((action: FetchGifsAction) => 
                from(fetchGifs(action.payload)).pipe(map((gifs: string[]) => giphyActionCreator.setGifs(gifs)))
            ),
        );
    }
    public fetchStickers = (action$: Observable<Action>) => {
        return action$.pipe(
            ofType(GiphyActionTypes.FETCH_STICKERS) as any,
            mergeMap((action: FetchStickersAction) => 
                from(fetchStickers(action.payload)).pipe(map((gifs: string[]) => giphyActionCreator.setStickers(gifs)))
            ),
        );
    }
    

    public getRootEpic = (): Epic[] => {
        return [this.fetchGifs, this.fetchStickers] as any
    }
}

export const giphyEpic = new GiphyEpic();