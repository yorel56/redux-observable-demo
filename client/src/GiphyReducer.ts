import { extend } from 'lodash';
import { GiphyState, GiphyStore, GiphyReducerKeys, UpdateGiphyReducerAction, GiphyActionTypes } from './GiphyReduxTypes';
import { IReducer } from './IReducer';

class GiphyReducer implements IReducer<GiphyState, GiphyStore> {

    public readonly initialState: GiphyState;
    private readonly initialValues: GiphyStore = {
        Gifs:[],
        Stickers: [],
        IsLoadingGifs: false,
    };

    public constructor() {
        this.initialState = { [GiphyReducerKeys.ReducerName]: this.initialValues };
    }

    public updateState = (state: GiphyStore | undefined, action: UpdateGiphyReducerAction): GiphyStore => {
        const incomingState: GiphyStore = state !== undefined ? state : this.initialValues;

        switch (action.type) {
            case GiphyActionTypes.SET_GIFS:
                return this.setGifs(incomingState, action.payload);
            case GiphyActionTypes.SET_IS_FETCHING_GIFS:
                return this.setIsLoadingGifs(incomingState, action.payload);
            case GiphyActionTypes.SET_STICKERS:
                return this.setStickers(incomingState, action.payload);
            default:
                return incomingState
        }
    };

    private setIsLoadingGifs = (store: GiphyStore, payload: boolean): GiphyStore => {
        return extend({}, store, {[GiphyReducerKeys.IsLoadingGifs]: payload})
    }
    private setGifs = (store: GiphyStore, payload: string[]): GiphyStore => {
        return extend({}, store, {[GiphyReducerKeys.Gifs]: payload})
    }
    private setStickers = (store: GiphyStore, payload: string[]): GiphyStore => {
        return extend({}, store, {[GiphyReducerKeys.Stickers]: payload})
    }
}

export const giphyReducer = new GiphyReducer();