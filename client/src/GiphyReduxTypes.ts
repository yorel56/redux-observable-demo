import { IAction } from "./IAction";

export enum GiphyReducerKeys {
    ReducerName = "Giphy",
    Gifs = "Gifs",
    IsLoadingGifs = "IsLoadingGifs",
    Stickers = "Stickers"
}

export interface GiphyStore {
    [GiphyReducerKeys.Gifs]: string[];
    [GiphyReducerKeys.Stickers]: string[];
    [GiphyReducerKeys.IsLoadingGifs]: boolean;
}

export interface GiphyState {
    [GiphyReducerKeys.ReducerName]: GiphyStore;
}

export enum GiphyActionTypes {
    FETCH_GIFS = "FETCH_GIFS",
    SET_GIFS = "SET_GIFS",
    FETCH_STICKERS = "FETCH_STICKERS",
    SET_STICKERS = "SET_STICKERS",
    SET_IS_FETCHING_GIFS = "SET_IS_FETCHING_GIFS"
}

export type FetchGifsAction = IAction<string, GiphyActionTypes.FETCH_GIFS>;
export type SetGifsAction = IAction<string[], GiphyActionTypes.SET_GIFS>;
export type FetchStickersAction = IAction<string, GiphyActionTypes.FETCH_STICKERS>;
export type SetStickersAction = IAction<string[], GiphyActionTypes.SET_STICKERS>;
export type SetIsFetchingGifsAction = IAction<boolean, GiphyActionTypes.SET_IS_FETCHING_GIFS>;

export type UpdateGiphyReducerAction = SetGifsAction | SetIsFetchingGifsAction | SetStickersAction;