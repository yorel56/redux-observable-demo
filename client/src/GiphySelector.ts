import { ApplicationReduxState } from './RootReducer';
import { createSelector, Selector } from "reselect";
import { GiphyReducerKeys } from './GiphyReduxTypes';

class GiphySelector {
    public readonly selectGifs: Selector<ApplicationReduxState, string[]>;
    public readonly selectStickers: Selector<ApplicationReduxState, string[]>;
    public readonly selectIsFetchingGifs: Selector<ApplicationReduxState, boolean>;

    public constructor() {
        this.selectGifs = createSelector(this.getGiphyStore, (store) => store[GiphyReducerKeys.Gifs]);
        this.selectStickers = createSelector(this.getGiphyStore, (store) => store[GiphyReducerKeys.Stickers]);
        this.selectIsFetchingGifs = createSelector(this.getGiphyStore, (store) => store[GiphyReducerKeys.IsLoadingGifs]);
    }

    private getGiphyStore = (state: ApplicationReduxState) => state.Giphy;
}

export const giphySelector = new GiphySelector();