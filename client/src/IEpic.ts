import { Epic } from "redux-observable";

export interface IEpic {
    getRootEpic: () => Epic[];
}