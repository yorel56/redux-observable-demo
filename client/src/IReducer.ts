import { IAction } from "./IAction";

export interface IReducer<State, Store> {
    initialState: State;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    updateState: <T extends IAction<any, any>>(state: Store | undefined, action: T) => Store;
}
