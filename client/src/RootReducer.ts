import { giphyReducer } from './GiphyReducer';
import { GiphyReducerKeys, GiphyStore } from './GiphyReduxTypes';
import { combineReducers } from "redux";


export const rootReducer = combineReducers({
    [GiphyReducerKeys.ReducerName]: giphyReducer.updateState,
});

export const initialState = {
    ...giphyReducer.initialState,
};

export interface ApplicationReduxState {
    [GiphyReducerKeys.ReducerName]: GiphyStore
}
