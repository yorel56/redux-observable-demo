import { createStore, applyMiddleware } from "redux";
import { createEpicMiddleware, combineEpics } from 'redux-observable';
import { giphyEpic } from "./GiphyEpic";
import { initialState, rootReducer } from "./RootReducer";
export const epicMiddleware = createEpicMiddleware();
const middleWares = [epicMiddleware];
// export const store = createStore(rootReducer, initialState, applyMiddleware(...middleWares));
export const rootEpic = combineEpics(
    ...giphyEpic.getRootEpic()
  );

export const  configureStore = () => {
    
    const store = createStore(rootReducer, initialState, applyMiddleware(...middleWares));

    epicMiddleware.run(rootEpic);

    return store;
}